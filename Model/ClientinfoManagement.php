<?php

namespace Firepush\Webpush\Model;

use Firepush\Webpush\Api\ClientinfoManagementInterface;
use Firepush\Webpush\Helper\Config;
use Magento\Store\Model\ScopeInterface;

class ClientinfoManagement implements ClientinfoManagementInterface
{

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    private $configWriter;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl $curl
     */
    private $curl;

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     */
    private $cacheTypeList;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlBuilder;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     *
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    public function __construct(
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\View\Element\Context $context
    ) {
        $this->configWriter = $configWriter;
        $this->curl = $curl;
        $this->cacheTypeList = $cacheTypeList;
        $this->urlBuilder = $context->getUrlBuilder();
        $this->scopeConfig = $context->getScopeConfig();
    }

    /**
     * {@inheritdoc}
     */
    public function postClientinfo($client_info)
    {
        $result = false;

        $clientHash = $client_info->getClientHash();
        $clientId = $client_info->getClientId();
        $clientAlias = $client_info->getClientAlias();
        if ($clientHash && $clientId && $clientAlias) {
            $params = [
                'client_id' => $clientId,
                'client_hash' => $clientHash,
                'base_url' => $this->urlBuilder->getUrl('', ['_secure' => true]),
                'timezone' => $this->scopeConfig->getValue('general/locale/timezone', ScopeInterface::SCOPE_STORE),
                'base_currency' => $this->scopeConfig->getValue('currency/options/base', ScopeInterface::SCOPE_STORE),
                'locale' => $this->scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE),
            ];

            $this->curl->post(Config::API_URL . '?action=verify_client', $params);
            $response = $this->curl->getBody();
            if ($response) {
                try {
                    $response = json_decode($response, 1);
                } catch (\Exception $e) {
                    $response = null;
                }
                if ($response && $response['success']) {
                    $this->configWriter->save(Config::XML_PATH_CLIENT_HASH, $clientHash);
                    $this->configWriter->save(Config::XML_PATH_CLIENT_ID, $clientId);
                    $this->configWriter->save(Config::XML_PATH_CLIENT_ALIAS, $clientAlias);

                    // NOTE not sure if all of them needs to be cleared
                    $this->cacheTypeList->cleanType('block_html');
                    $this->cacheTypeList->cleanType('layout');
                    $this->cacheTypeList->cleanType('config');
                    $this->cacheTypeList->cleanType('full_page');

                    $result = true;
                }
            }
        }

        return $result;
    }
}
