<?php
/**
 * Created by PhpStorm.
 * User: Mykolas
 * Date: 2017-02-07
 * Time: 11:49
 */

namespace Firepush\Webpush\Model;

use \Firepush\Webpush\Helper\Config;
use Magento\Customer\Model\Customer;
use Magento\Sales\Model\Order;
use \Magento\Store\Model\ScopeInterface;

class FirepushInfo
{

    /**
     * @var string
     */
    private $scope;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    private $cookieManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->scope = ScopeInterface::SCOPE_STORE;
        $this->cookieManager = $cookieManager;
    }

    public function getStoreInfo()
    {

        $configValues = [];

        $configsToCollect = [
            'timezone' => 'general/locale/timezone',
            'baseCurrency' => 'currency/options/base',
            'storeName' => 'general/store_information/name',
            'singleStore' => 'general/single_store_mode/enabled',
            'secureBaseUrl' => 'web/secure/base_url',
            'generalEmail' => 'trans_email/ident_general/email',
            'contactEmail' => 'contact/email/recipient_email',
            'countryId' => 'general/store_information/country_id',
            'phone' => 'general/store_information/phone',
            'secureStorefrontendUrls' => 'web/secure/use_in_frontend',
        ];

        foreach ($configsToCollect as $configVar => $configPath) {
            $configValues[$configVar] = $this->scopeConfig->getValue($configPath, $this->scope);
        }

        return $configValues;
    }

    /**
     * Retrieves stored firepush client hash
     * @return mixed
     */
    public function getFirepushClientHash()
    {
        return $this->scopeConfig->getValue(Config::XML_PATH_CLIENT_HASH, $this->scope);
    }

    /**
     * Retrieves stored firepush client alias
     * @return mixed
     */
    public function getFirepushClientAlias()
    {
        return $this->scopeConfig->getValue(Config::XML_PATH_CLIENT_ALIAS, $this->scope);
    }

    /**
     * Retrieves stored firepush client id
     * @return mixed
     */
    public function getFirepushClientId()
    {
        return $this->scopeConfig->getValue(Config::XML_PATH_CLIENT_ID, $this->scope);
    }

    /**
     * Retrieves stored shared secret value
     * @return mixed
     */
    public function getSharedSecret()
    {
        return Config::SHARED_SECRET;
    }

    /**
     * Retrieves stored firepush worker version
     * @return mixed
     */
    public function getFirepushWorkerVersion()
    {
        return $this->scopeConfig->getValue(Config::XML_PATH_WORKER_VERSION, $this->scope);
    }

    /**
     * Checks if plugin frontend is enabled
     * @return bool
     */
    public function isFirepushFrontendEnabled()
    {
        return (bool)$this->scopeConfig->getValue(Config::XML_PATH_FRONTEND_ENABLED, $this->scope);
    }

    public function addPendingAction($name, $data = null)
    {
        $pendingActions = &$this->getPendingActions();
        $pendingActions[] = ['name' => $name, 'data' => $data];
        $this->cookieManager->setSensitiveCookie(Config::COOKIE_PENDING_ACTIONS, json_encode($pendingActions));
    }

    public function getPendingActions()
    {
        $pendingActions = $this->cookieManager->getCookie(Config::COOKIE_PENDING_ACTIONS);
        try {
            $pendingActions = $pendingActions ? json_decode($pendingActions, true) : [];
        } catch (\Exception $e) {
            $pendingActions = [];
        }
        return $pendingActions;
    }

    public function clearPendingActions()
    {
        $this->cookieManager->deleteCookie(Config::COOKIE_PENDING_ACTIONS);
    }

    public function getCartToken($cart_id)
    {
        return md5($cart_id . Config::SHARED_SECRET);
    }

    public function escapeForJavascript($value)
    {
        $value = preg_replace_callback('~([^a-zA-Z0-9])~i', function ($matches) {
            return '\\u' . sprintf('%04s', dechex(ord($matches[0])));
        }, $value);

        return $value;
    }

    public function escapeForJson($value)
    {
        $value = preg_replace('~<~i', '\\u003c', $value);

        return $value;
    }

    public function getOrderCustomerId(Order $order)
    {
        $result = null;

        if ($order->getCustomerIsGuest()) {
            $result = $this->getCustomerId();
        } else {
            $result = $order->getCustomerId();
        }

        return $result;
    }

    /**
     * @return null|string
     */
    public function getCustomerId()
    {
        return $this->cookieManager->getCookie('fp_token');
    }
}
