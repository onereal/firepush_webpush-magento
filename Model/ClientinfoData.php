<?php
/**
 * Created by PhpStorm.
 * User: Mykolas
 * Date: 2017-07-27
 * Time: 11:05
 */

namespace Firepush\Webpush\Model;

class ClientinfoData implements \Firepush\Webpush\Api\Data\ClientinfoDataInterface
{
    /**
     * @var string $client_hash
     */
    private $client_hash;

    /**
     * @var string $client_alias
     */
    private $client_alias;

    /**
     * @var int $client_id
     */
    private $client_id;

    /**
     * @inheritdoc
     */
    public function setClientHash($hash)
    {
        $this->client_hash = $hash;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getClientHash()
    {
        return $this->client_hash;
    }

    /**
     * @inheritdoc
     */
    public function setClientAlias($alias)
    {
        $this->client_alias = $alias;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getClientAlias()
    {
        return $this->client_alias;
    }

    /**
     * @inheritdoc
     */
    public function setClientId($id)
    {
        $this->client_id = $id;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getClientId()
    {
        return $this->client_id;
    }
}
