<?php

namespace Firepush\Webpush\Observer\Controller;

use Firepush\Webpush\Helper\Config;
use Firepush\Webpush\Helper\FirepushApi;
use Firepush\Webpush\Model\FirepushInfo;
use Magento\Catalog\Model\Product;

class ActionCatalogProductSaveEntityAfter implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var FirepushInfo
     */
    private $firepushInfo;

    /**
     * @var FirepushApi
     */
    private $firepushApi;

    public function __construct(
        FirepushInfo $firepushInfo,
        FirepushApi $firepushApi
    ) {
        $this->firepushInfo = $firepushInfo;
        $this->firepushApi = $firepushApi;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $controller = $observer->getData('controller');

        /**
         * @var Product $product
         */
        $product = $observer->getData('product');
        $productData = $product->getData();
        $variant_id = $productData['entity_id'];

        $webhook_data = [
            'id' => $variant_id,
            'image' => ['src' => $product->getMediaGalleryImages()->getFirstItem()->getData('url')],
            'title' => $productData['name'],
            'handle' => $productData['sku'],
            'variants' => [['id' => $variant_id, 'price' => (float)$product->getFinalPrice()]],
        ];
        $response = $this->firepushApi->callWebhook('product_update', $webhook_data);
    }
}
