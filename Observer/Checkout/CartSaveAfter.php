<?php

namespace Firepush\Webpush\Observer\Checkout;

use Firepush\Webpush\Helper\Config;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Session\Proxy as CheckoutSession;

class CartSaveAfter implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Firepush\Webpush\Model\FirepushInfo
     */
    private $firepushInfo;

    /**
     * @var \Magento\Framework\EntityManager\EntityManager
     */
    private $entityManager;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    public function __construct(
        \Firepush\Webpush\Model\FirepushInfo $firepushInfo,
        \Magento\Framework\EntityManager\EntityManager $entityManager,
        CheckoutSession $checkoutSession
    ) {
        $this->firepushInfo = $firepushInfo;
        $this->entityManager = $entityManager;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {

        /** @var \Magento\Checkout\Model\Cart $cart */
        $cart = $observer->getData('cart');
        $cartItems = $cart->getQuote()->getAllVisibleItems();

        $firstProduct = ($cartItems && count($cartItems) > 0) ? $cartItems[0]->getProduct() : null;

        $product_image = '';
        $product_title = '';
        $product_price = 0;

        if ($firstProduct) {
            $firstProduct->load($firstProduct->getData('entity_id'));
            $product_title = $firstProduct->getData('name');
            $product_price = (float)$firstProduct->getFinalPrice() * 100;

            $media_gallery_images = $firstProduct->getMediaGalleryImages();
            if ($media_gallery_images) {
                $product_image = $media_gallery_images->getFirstItem()->getData('url');
            }
        }

        $cart_data = [
            'token' => $this->firepushInfo->getCartToken($this->checkoutSession->getQuoteId()),
            'items_count' => $cart->getQuote()->getItemsQty(),
//                'items' => [],
            'product_title' => $product_title,
            'product_price' => $product_price,
            'product_image' => $product_image,
        ];

        $this->checkoutSession->setData('cart_update_data', $cart_data);
    }
}
