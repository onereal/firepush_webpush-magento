<?php

namespace Firepush\Webpush\Observer\Checkout;

use Firepush\Webpush\Helper\FirepushApi;
use Firepush\Webpush\Model\FirepushInfo;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SubmitAllAfter implements ObserverInterface
{

    /**
     * @var FirepushApi
     */
    private $firepushApi;

    /**
     * @var FirepushInfo
     */
    private $firepushInfo;

    public function __construct(
        FirepushApi $firepushApi,
        FirepushInfo $firepushInfo
    ) {
        $this->firepushApi = $firepushApi;
        $this->firepushInfo = $firepushInfo;
    }

    /**
     * Execute observer
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(
        Observer $observer
    ) {
        $order = $observer->getData('order');
//        $quote = $observer->getData('quote');
        $created_at = $order->getCreatedAt();
        $cart_id = $order->getQuoteId();

        $order_items = $order->getAllItems();
        $line_items = [];

        foreach ($order_items as $order_item) {
            $line_items[] = [
                'product_id' => $order_item->getProductId(),
                'title' => $order_item->getName(),
                'price' => $order_item->getPrice(),
            ];
        }

        $webhook_data = [
            'id' => $order->getId(),
            'cart_token' => $this->firepushInfo->getCartToken($cart_id),
            'customer' => [
                'id' => $this->firepushInfo->getCustomerId(),
            ],
            'line_items' => &$line_items,
            'total_price' => $order->getGrandTotal(),
            'total_price_usd' => null, // calculated on firepush side if not passed here
            'currency' => $order->getOrderCurrencyCode(),
            'created_at' => $created_at,
        ];
        $response = $this->firepushApi->callWebhook('order_creation', $webhook_data);
    }
}
