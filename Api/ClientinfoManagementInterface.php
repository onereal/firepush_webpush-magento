<?php


namespace Firepush\Webpush\Api;

interface ClientinfoManagementInterface
{

    /**
     * POST for client_info api
     * @param \Firepush\Webpush\Api\Data\ClientinfoDataInterface $client_info
     * @return boolean
     */
    public function postClientinfo($client_info);
}
