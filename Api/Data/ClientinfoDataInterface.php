<?php
/**
 * Created by PhpStorm.
 * User: Mykolas
 * Date: 2017-07-27
 * Time: 11:02
 */

namespace Firepush\Webpush\Api\Data;

interface ClientinfoDataInterface
{
    /**
     * @param string $hash
     * @return $this
     */
    public function setClientHash($hash);

    /**
     * @return string
     */
    public function getClientHash();

    /**
     * @param string $alias
     * @return this
     */
    public function setClientAlias($alias);

    /**
     * @return string
     */
    public function getClientAlias();

    /**
     * @param int $id
     * @return $this
     */
    public function setClientId($id);

    /**
     * @return int
     */
    public function getClientId();
}
