<?php
    /**
     * Created by PhpStorm.
     * User: Mykolas
     * Date: 2017-02-03
     * Time: 11:26
     */

    use \Magento\Framework\Component\ComponentRegistrar;

    ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Firepush_Webpush', __DIR__);
