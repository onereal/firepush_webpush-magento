define([
    'jquery'
], function ($) {
    'use strict';

    return {

        config : {},
        setup : function ( config ) {
            this.config = config;
        },
        updateCart : function ( data ) {
            console.log("::--update cart--:: data: ", data);

            FirePush.notify_cart_update(data);
            // TODO pass shop, ct, fpt

            /*$.ajax({
                type: 'POST',
                url: this.config.api_link + '?action=update_cart&shop='+this.config.client_alias,
                dataType: "text",
                data: data,
                success: function (response) {
                    console.log( "response: ", response );
                }
            });*/

        }

    };

});

