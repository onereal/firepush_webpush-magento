<?php

namespace Firepush\Webpush\Controller;

use Firepush\Webpush\Helper\Config;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;

class Router implements RouterInterface
{

    /** @var \Magento\Framework\App\ActionFactory $actionFactory */
    private $actionFactory;

    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory
    ) {
        $this->actionFactory = $actionFactory;
    }

    /**
     * @inheritdoc
     */
    public function match(RequestInterface $request)
    {
        $filename = trim($request->getPathInfo(), "/");
        if ($filename == Config::SERVICEWORKER_ENDPOINT) {
            $request
                ->setModuleName("fpworker")
                ->setControllerName("index")
                ->setActionName("worker");
        }

        return $this->actionFactory->create('Magento\Framework\App\Action\Forward');
    }
}
