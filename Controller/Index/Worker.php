<?php

namespace Firepush\Webpush\Controller\Index;

class Worker extends \Magento\Framework\App\Action\Action
{
    /** @var \Magento\Framework\View\Result\LayoutFactory $layoutFactory */
    private $layoutFactory;

    /**
     * Index constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\LayoutFactory $layoutFactory
    ) {
        $this->layoutFactory = $layoutFactory;
        parent::__construct($context);
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        return $this->layoutFactory->create()
            ->addDefaultHandle()
            ->setHeader("Content-Type", "text/javascript", true)
            ->setHeader("Service-Worker-Allowed", "/", true)
            ->setHeader("Pragma", "public", true)
            ->setHeader("Cache-Control", "public", true)
            ->setHeader("Cache-Control", "max-age=5");
    }
}
