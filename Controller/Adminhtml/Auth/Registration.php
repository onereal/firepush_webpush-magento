<?php

namespace Firepush\Webpush\Controller\Adminhtml\Auth;

use Firepush\Webpush\Helper\Config;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 * @package Firepush\Webpush\Controller\Adminhtml\Auth\Registration
 */
class Registration extends \Magento\Backend\App\Action
{

    /**
     * Index resultPageFactory
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * Registration constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Function execute
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $callback_url = $this->storeManager->getStore()->getBaseUrl() . 'rest/default/V1/firepush-webpush/client_info';
        $url = Config::BASE_URL . "en/registration/?client_type=3&callback_url=" . rawurlencode($callback_url)
            . "&magento_plugin_version=" . Config::PLUGIN_VERSION;
        $this->_redirect($url);
        return $this->resultPageFactory->create();
    }
}
