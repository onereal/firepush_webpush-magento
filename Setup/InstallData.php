<?php
/**
 * Created by PhpStorm.
 * User: Mykolas
 * Date: 2017-02-06
 * Time: 16:19
 */

namespace Firepush\Webpush\Setup;

use Firepush\Webpush\Helper\Config;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallData implements InstallDataInterface
{

    private $scopeConfig;
    private $logger;
    private $configWriter;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
    ) {
    
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->configWriter = $configWriter;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->logger->debug('Firepush data installation started.');

        $this->configWriter->save(Config::XML_PATH_CLIENT_HASH, '');
        $this->configWriter->save(Config::XML_PATH_CLIENT_ALIAS, '');
        $this->configWriter->save(Config::XML_PATH_WORKER_VERSION, 1);
        $this->configWriter->save(Config::XML_PATH_FRONTEND_ENABLED, 1);

        $setup->endSetup();
    }
}
