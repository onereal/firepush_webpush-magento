<?php

namespace Firepush\Webpush\Setup;

use Firepush\Webpush\Helper\Config;

class Uninstall implements \Magento\Framework\Setup\UninstallInterface
{
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    private $configWriter;

    public function __construct(
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
    ) {
    
        $this->configWriter = $configWriter;
    }

    /**
     * Module uninstall code
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @return void
     */
    public function uninstall(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $setup->startSetup();

        // Uninstall logic here
        $this->configWriter->save(Config::XML_PATH_FRONTEND_ENABLED, 0);

        $setup->endSetup();
    }
}
