<?php

namespace Firepush\Webpush\Block;

class Manifest extends \Magento\Framework\View\Element\Template
{

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data
    ) {
        $data["cache_lifetime"] = 60 * 60 * 24 * 2;
        parent::__construct($context, $data);
    }
}
