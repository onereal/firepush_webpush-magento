<?php

namespace Firepush\Webpush\Block\Script;

use \Firepush\Webpush\Helper\Config;

class Worker extends \Magento\Framework\View\Element\Template
{
    const VERSION_PREFIX = "fpsw";

    /**
     * @var \Firepush\Webpush\Model\FirepushInfo $firepushInfo
     */
    private $firepushInfo;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Firepush\Webpush\Model\FirepushInfo $firepushInfo,
        array $data
    ) {
        $data["cache_lifetime"] = 5;
        $this->firepushInfo = $firepushInfo;
        parent::__construct($context, $data);
    }

    /**
     * Get the service worker version string.
     *
     * @return string
     */
    public function getVersion()
    {
        return implode("-", [
            static::VERSION_PREFIX,
            time()
        ]);
    }

    /**
     * Get the list of URLs for external scripts to import into the service worker.
     *
     * @return string[]
     */
    public function getExternalScriptUrls()
    {
        $firepushClientHash = $this->firepushInfo->getFirepushClientHash();
        if ($firepushClientHash) {
            $scripts = [
                Config::SCRIPTS_BASE_URL . $firepushClientHash .'/worker.js'
            ];
        } else {
            $scripts = [];
        }

        return array_filter($scripts);
    }

    public function getWorkerVersion()
    {
        return $this->firepushInfo->getFirepushWorkerVersion();
    }
}
