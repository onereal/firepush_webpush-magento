<?php

namespace Firepush\Webpush\Block\Script;

use \Firepush\Webpush\Helper\Config;
use \Magento\Checkout\Model\Session\Proxy as CheckoutSession;
use \Magento\Customer\Model\Session\Proxy as CustomerSession;

class FirepushShopScript extends \Magento\Framework\View\Element\Template
{

    /** @var Config $config */
    private $config;

    /**
     * @var \Firepush\Webpush\Model\FirepushInfo $firepushInfo
     */
    private $firepushInfo;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data,
        Config $config,
        \Firepush\Webpush\Model\FirepushInfo $firepushInfo,
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
        $this->firepushInfo = $firepushInfo;
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
    }

    public function isEnabled()
    {
        return $this->firepushInfo->isFirepushFrontendEnabled() && (bool)$this->firepushInfo->getFirepushClientHash();
    }

    public function getServiceWorkerJsUrl()
    {
        return $this->_urlBuilder->getDirectUrl(Config::SERVICEWORKER_ENDPOINT);
    }

    public function getManifestUrl()
    {
        return $this->_urlBuilder->getDirectUrl(Config::MESSAGING_MANIFEST);
    }

    public function getEmbedJsUrl()
    {
        $scritp_url = Config::SCRIPTS_BASE_URL . $this->firepushInfo->getFirepushClientHash() . '/' . 'firepush.min.js';
        return $scritp_url;
    }

    public function getPendingActionsJSON($clear = true)
    {

        $pendingActions = $this->firepushInfo->getPendingActions();

        if ($clear) {
            $this->firepushInfo->clearPendingActions();
        }

        return $this->firepushInfo->escapeForJson(json_encode($pendingActions, JSON_HEX_APOS | JSON_HEX_QUOT));
    }

    public function getUpdatedCartData()
    {
        $cart_data = $this->checkoutSession->getData('cart_update_data', true);
        if ($cart_data) {
            $result = $this->firepushInfo->escapeForJson(json_encode($cart_data, JSON_HEX_APOS | JSON_HEX_QUOT));
        } else {
            $result = null;
        }

        return $result;
    }

    public function getConfigData()
    {
        $result = [
            'api_link' => Config::BASE_URL . 'en/api/',
            'client_alias' => $this->firepushInfo->getFirepushClientAlias(),
        ];

        return $this->firepushInfo->escapeForJson(json_encode($result, JSON_HEX_APOS | JSON_HEX_QUOT));
    }

    /**
     * Returns customer id string or null if not set yet
     * @return null|string
     */
    public function getCustomerId()
    {
        return $this->firepushInfo->getCustomerId();
    }
}
