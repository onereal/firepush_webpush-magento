<?php

namespace Firepush\Webpush\Helper;

use Firepush\Webpush\Model\FirepushInfo;

class FirepushApi extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var FirepushInfo
     */
    private $firepushInfo;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        FirepushInfo $firepushInfo
    ) {
        parent::__construct($context);
        $this->firepushInfo = $firepushInfo;
    }

    /**
     * Call firepush webhook
     * @param string $webhook
     * @param array $call_data
     * @return bool
     */
    public function callWebhook($webhook, $call_data)
    {

        $call_data_string = json_encode($call_data);
        // hash_hmac available for PHP >= 5.1.2
        $data_hmac = base64_encode(
            hash_hmac('sha256', $call_data_string, $this->firepushInfo->getSharedSecret(), true)
        );

        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            Config::API_URL . '?webhook=' . $webhook . '&client_id=' . $this->firepushInfo->getFirepushClientId()
        );
        /*curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            ['Content-Type: application/json', 'FIREPUSH-HMAC-SHA256:'.$data_hmac, 'XDEBUG_SESSION:Mike']
        );*/
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            ['Content-Type: application/json', 'FIREPUSH-HMAC-SHA256:' . $data_hmac]
        );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $call_data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        $result = false;
        if ($response) {
            try {
                $response = json_decode($response, 1);
            } catch (\Exception $e) {
                $response = null;
            }
            if ($response && !isset($response['error'])) {
                $result = true;
            }
        }

        return $result;
    }
}
