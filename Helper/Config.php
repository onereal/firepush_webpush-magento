<?php

namespace Firepush\Webpush\Helper;

class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    const PLUGIN_VERSION = '1.0.1';
    const XML_PATH_CLIENT_HASH = 'web/firepush/client_hash';
    const XML_PATH_CLIENT_ALIAS = 'web/firepush/client_alias';
    const XML_PATH_CLIENT_ID = 'web/firepush/client_id';
    const XML_PATH_WORKER_VERSION = 'web/firepush/worker_version';
    const XML_PATH_FRONTEND_ENABLED = 'web/firepush/frontend_enabled';
    const XML_SHARED_SECRET = 'web/firepush/shared_secret';
    const COOKIE_PREFIX = 'fp_';
    const COOKIE_PENDING_ACTIONS = self::COOKIE_PREFIX . 'pending_actions';
    const PATH_WILDCARD_SYMBOL = '*';
    const SERVICEWORKER_ENDPOINT = 'fp_worker.js';
    const MESSAGING_MANIFEST = 'fp_manifest.json';
    const BASE_URL = 'https://admin.firepush.io/';
    const API_URL = self::BASE_URL . 'en/magento/';
    const SCRIPTS_BASE_URL = self::BASE_URL . 'sdk/magento/';
    const SHARED_SECRET = 'g1sf5v35qbxp4eholy8kk23v4b9u3abw';
}
